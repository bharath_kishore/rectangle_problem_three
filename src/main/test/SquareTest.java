import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class SquareTest {
    public int side;
    Square squareObject;
    @Test
    void shouldReturnAreaAsFourIfSideIsTwo()
    {
        side=2;

        squareObject=new Square(side);

        int actual=squareObject.area();

        assertEquals(4,actual);
    }
    @Test
    void shouldReturnPerimeterAsSixteenIfSideIsFour(){
        side=4;

        squareObject=new Square(side);

        int actual=squareObject.perimeter();

        assertEquals(16,actual);

    }
}
