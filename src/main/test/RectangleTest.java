import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class RectangleTest {
    public int length=0;
    public int breadth=0;

    public Rectangle rectangleObject;
    @Tag("area")
    @Test
    void shouldReturnAreaAsTwentyIfTheLengthIsFourAndBreadthIsFive()
    {
         length=4;
         breadth=5;

        rectangleObject=new Rectangle(length,breadth);

        int actual=rectangleObject.area();

        assertEquals(20,actual);

    }
    @Tag("area")
    @Test
    void shouldReturnAreaAsFortyTwoIfTheLengthIsSixAndBreadthIsSeven()
    {
        length=6;
        breadth=7;

        rectangleObject=new Rectangle(length,breadth);

       int actual=rectangleObject.area();
       assertEquals(42,actual);

    }

    @Tag("perimeter")
    @Test
    void shouldReturnPerimeterAsEighteenIfTheLengthIsFiveAndBreadthIsFour(){
        length=5;
        breadth=4;

        rectangleObject =new Rectangle(length,breadth);

        int actual=rectangleObject.perimeter();

        assertEquals(18,actual);

    }


}